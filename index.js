function formatString(value, args, values) {
  value = value.replace(/\$\{(\w+)\}/g, (match, key) => format(key, args, values))
  return args.reduce((str, arg) => {
    if (typeof arg === 'number') return str.replace(/%n/, arg)
    if (typeof arg === 'string') return str.replace(/%s/, arg)
    if (typeof arg === 'object') {
      return str.replace(/%\{(\w+)\}/g, (match, key) => arg[key] || '')
    }
  }, value)
}

function findCorrectPluralization(arrayOfPluralizations, args) {
  const num = (typeof args[0] === 'number') ? args[0] : 0
  const value = arrayOfPluralizations.find(([start, end]) => (start === null || num >= start) && (end === null || num <= end))
  return value ? value[2] : ''
} 

function format(key, args, values) {
  let value = values[key]
  if (Array.isArray(value)) {
    value = findCorrectPluralization(value, args)
  }
  return formatString(value, args, values)
}

module.exports = function(values) {
  return (key, ...args) => format(key, args, values)
}