# a-i18n

Inspired by http://i18njs.com/

## Install

```npm install --save a-i18n```

```const i18n = require('a-i18n')```

## Usage

### Simple

```
const en = i18n({ key: 'my message' })
const str = en('key')
console.log(str) // my message
```

### Interpolation

You can interpolate numbers and strings using **%n** and **%s**.

```
const en = i18n({ 
  hello: 'Hello %s', unread_messages: 'You have %n unread messages' 
})

en('hello', 'World') // Hello World
en('unread_messages', 4) // You have 4 unread messages
```

Interpolation will match by the first argument against the first token of the matching type.

```
const en = i18n({
  test: '%s %n %s %s %n'
})
en('test', 1, 2, 'a', 'b') // a 1 b %s 2
```

Note that the third "%s" was left because no argument matched it.

**TODO** Add option for user to specify how it should be handled (e.g. throw exception, replace with blank string, replace with default string specified by user)

### Pluralization

As you noticed in the above example for unread messages, that message wouldn't look very nice if I only had 1 unread message.

```
const en = i18n({ 
  unread_messages: [
    [0, 0, 'You have no unread messages'], 
    [1, 1, 'You have %n unread message'], 
    [2, null, 'You have %n unread messages']] 
})
en('unread_messages', 1) // You have 1 unread message
en('unread_messages', 2) // You have 2 unread messages
```

Note that pluralization always assumes that the first argument controls pluralization. 

```
const en = i18n({ 
  any: [
    [0, 0, '%s have no messages.'], 
    [1, null, '%s have messages.']
  ] 
})
en('any', 1, 'You') // This works
en('any', 'You', 1) // This would look at 'You' and since it is not a number default pluralization to 0
```

### Named parameters

```
const en = i18n({ 
  welcome: 'Welcome %{name}! Your last visit was on %{lastVisit}.' 
})
en('welcome', { name: 'Andreas', lastVisit: '2017-01-01' }) // Welcome Andreas! Your last visit was on 2017-01-01.
```

### Reference other messages

```
const en = i18n({ 
  hello: 'Hello %s! ${any_messages}', 
  any_messages: [
    [0, 0, 'You have no messages.'], 
    [1, 1, 'You have %n message.'], 
    [2, null, 'You have %n messages.']
  ]
})
en('hello', 1, 'Andreas') // Hello Andreas! You have 1 message.
```

### Multiple languages

```
const en = i18n({ hello: 'Hello!' })
const sv = i18n({ hello: 'Hej!' })
```

## Planned improvements

See https://gitlab.com/amcd/i18n/issues