const i18n = require('./index')

test('should return a method', () => {
  const fn = i18n({})
  expect(typeof fn).toBe('function')
})

test('should return string by key', () => {
  const values = {
    hello: 'Hello World'
  }
  const fn = i18n(values)
  expect(fn('hello')).toBe(values.hello)
})

test('should return string with replacements', () => {
  const values = {
    hello: 'Hello %s'
  }
  const fn = i18n(values)
  expect(fn('hello', 'Andreas')).toBe('Hello Andreas')  
})

test('should return string with pluralization', () => {
  const values = {
    messages: [
      [0, 0, 'You have 0 unread messages'],
      [1, 1, 'You have 1 unread message'],
      [2, null, 'You have %n unread messages']
    ]
  }
  const fn = i18n(values)
  expect(fn('messages', 0)).toBe('You have 0 unread messages')
  expect(fn('messages', 1)).toBe('You have 1 unread message')
  expect(fn('messages', 2)).toBe('You have 2 unread messages')
})

test('should return string with named replacement', () => {
  const values = {
    hello: 'Hello %{name}. Today is %{weekday}'
  }
  const fn = i18n(values)
  expect(fn('hello', {name: 'Andreas', weekday: 'Monday'})).toBe('Hello Andreas. Today is Monday')
})

test('should use reference to other string', () => {
  const values = {
    count: [
      [null, 1, 'first'],
      [2, 2, 'second'],
      [3, 3, 'third'],
      [4, null, '%nth']
    ],
    hello: 'Hello %s. You are the ${count} customer to buy %{product} today.'
  }
  const fn = i18n(values)
  expect(fn('hello', 1, 'Andreas', {product: 'ice cream'})).toBe('Hello Andreas. You are the first customer to buy ice cream today.')
  expect(fn('hello', 2, 'Peter', {product: 'crackers'})).toBe('Hello Peter. You are the second customer to buy crackers today.')
  expect(fn('hello', 3, 'Mary', {product: 'shoes'})).toBe('Hello Mary. You are the third customer to buy shoes today.')
  expect(fn('hello', 10, 'Denise', {product: 'laptops'})).toBe('Hello Denise. You are the 10th customer to buy laptops today.')
})